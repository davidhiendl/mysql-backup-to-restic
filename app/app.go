package app

import (
	"database/sql"
	"gitlab.com/davidhiendl/mysql-backup-to-restic/app/config"
	"gitlab.com/davidhiendl/mysql-backup-to-restic/app/restic"
	"github.com/sirupsen/logrus"
	"os"
	"path/filepath"
)

type App struct {
	config  *config.Config
	db      *sql.DB
	restic  *restic.Restic
	dumpDir string
}

// Create new config and populate it from environment
func NewApp(config *config.Config) *App {
	app := App{
		config:  config,
		dumpDir: filepath.Join(config.Common.ScratchDir, "sqldumps"),
	}

	app.db = app.connectToDb("")
	app.restic = restic.New(config)

	return &app
}

// run templates against containers and generate config
func (app *App) Run() {
	logrus.Info("Connecting to database ...")
	databases := app.getDatabases()

	dumpedFiles := make([]string, 0)

	logrus.Info("Starting database dump ...")

	// export databases
	for db, ok := range databases {
		if !ok {
			continue
		}

		// execute dump
		dumpFile := app.dumpDatabaseMysqldump(db)
		dumpedFiles = append(dumpedFiles, dumpFile)
	}

	logrus.Info("Starting restic backup ...")

	// init repo
	err := app.restic.InitRepositoryIfAbsent()
	if err != nil {
		logrus.Fatalf("failed to execute restic init: %+v", err)
	}

	// run backup
	err = app.restic.Backup(app.dumpDir, nil)
	if err != nil {
		logrus.Fatalf("failed to execute restic backup: %+v", err)
	}

	// run cleanup
	if app.config.RetentionPolicy.HasKeepDirective() {
		err := app.restic.Forget(&app.config.RetentionPolicy)
		if err != nil {
			logrus.Fatalf("failed to execute restic forget: %+v", err)
		}

		if app.config.RetentionPolicy.Check {
			err = app.restic.Check()
			if err != nil {
				logrus.Fatalf("failed to execute restic forget: %+v", err)
			}
		}
	}

	logrus.Info("Cleaning up temporary backup files ...")

	// clean up
	for _, file := range dumpedFiles {
		err := os.Remove(file)
		if err != nil {
			logrus.Warnf("failed to cleanup backup file: %+v error: %+v", file, err)
		}
	}

	logrus.Info("Backup completed.")
}

func (app *App) ShouldIncludeDatabase(name string) (bool, string) {
	// exclude system
	if app.config.Databases.ExcludeSystem {
		if name == "performance_schema" || name == "information_schema" || name == "mysql" {
			return false, "system database excluded"
		}
	}

	// exclude specific list
	for _, exclude := range app.config.Databases.Exclude {
		if name == exclude {
			return false, "on exclude list"
		}
	}

	// if include list enabled
	if len(app.config.Databases.Include) > 0 {
		for _, include := range app.config.Databases.Include {
			if name == include {
				return true, "on include list"
			}
		}

		// only allow databases that are included
		return false, "not on include list"
	}

	return true, "not on any list"
}

func (app *App) Close() {
	app.db.Close()
}
